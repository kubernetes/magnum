# base on the heat image as there's not train for magnum
FROM docker.io/openstackhelm/heat:train-ubuntu_bionic

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
        apt-get install -y \
		git \
        patch \
        wget && \
        apt-get clean

# dependencies specific to magnum, not available in the heat image
RUN pip install -c https://opendev.org/openstack/requirements/raw/tag/train-eol/upper-constraints.txt \
        kubernetes \
        marathon \
        werkzeug \
        wsme

RUN mkdir /tar && cd /tar && wget https://tarballs.openstack.org/magnum/magnum-9.2.0.tar.gz && tar zxf magnum-9.2.0.tar.gz

COPY . /code
RUN cd /code && git format-patch --no-signature -N --full-index 9.2.0 && rm 0003*patch
RUN cd /tar/magnum-9.2.0 && for p in $(ls /code/*.patch); do patch -p1 < $p; done && python setup.py install && cp -R /tar/magnum-9.2.0/magnum/* /var/lib/openstack/lib/python3.6/site-packages/magnum

# change base user from heat to magnum
RUN groupmod -n magnum heat && usermod -l magnum -d /var/lib/magnum heat && mv /var/lib/heat /var/lib/magnum

# do not run as root
USER magnum
