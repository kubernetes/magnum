# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy
# of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from oslo_config import cfg
from magnum.i18n import _

oidc_group = cfg.OptGroup(name='oidc',
                           title='Oidc options for the magnum services')

oidc_opts = [
    cfg.StrOpt('applicationURL',
               help=_('The api url that we need for application creation and deletion')),
    cfg.StrOpt('registerURL',
               help=_('The api url for a new sso registration')),
    cfg.StrOpt('getTokenURL',
               help=_('The api url that points to keycloak')),
    cfg.StrOpt('identityURL',
               help=_('The api url that we need to get the user id for each user')),
    cfg.StrOpt('client_id',
               help=_('The client id of our manager application')),
    cfg.StrOpt('client_secret', secret=True,
               help=_('The client secret of our manager application')),
    cfg.StrOpt('audience',
               help=_('The audience that the API service is using')),
    cfg.StrOpt('managerId',
               help=_('The id of our manager application')),

    # The following params are for configuring the API server in the master node
    cfg.StrOpt('issuer_url',
               help=_('The url of our oidc issuer')),
    cfg.StrOpt('username_claim',
               help=_('The username field')),
    cfg.StrOpt('groups_claim',
               help=_('The groups field')),
    cfg.StrOpt('username_prefix',
               help=_('A prefix for each username')),
    cfg.StrOpt('groups_prefix',
               help=_('A prefix for each group'))
]

def register_opts(conf):
    conf.register_group(oidc_group)
    conf.register_opts(oidc_opts, group=oidc_group)


def list_opts():
    return {
        oidc_group: oidc_opts
    }
