# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import mock
from requests import Response
from magnum.tests import base
from magnum.cern import oidc
from magnum.common import exception


class TestOIDC(base.TestCase):

    # This method will be used by the mock to replace json.loads
    def mock_call(mode):
        def mocked_json_loads(*args, **kwargs):

            if mode == "get_token":
                return {"access_token": "test"}

            elif mode == "valid_id":
                return {"data": [{"id": "TEST12345678901234567890123456789010"}]}

            elif mode == "invalid_id":
                return {"data": [{"id": "1nv@l1d"}]}

            elif mode == "empty_data":
                return {"data": []}

        return mocked_json_loads

    @mock.patch('magnum.cern.oidc.requests.post')
    @mock.patch('magnum.cern.oidc.requests.get')
    def test_oidc(self, mock_get, mock_post):

        # init oidc
        with mock.patch('magnum.cern.oidc.json.loads', side_effect=TestOIDC.mock_call("get_token")):

            mock_response = Response()
            mock_response.status_code = 200
            mock_post.return_value = mock_response

            oidc_application = oidc.OidcApplication()

            oidc_application.identityURL = "http://test.test.test.test"
            oidc_application.applicationURL = "http://test.test.test.test"
            oidc_application.token = "test"
            oidc_application.headers = {
                "accept": "*/*",
                "Authorization": "Bearer test",
                "Content-Type": "application/json"
            }

            # test valid username/application id
            with mock.patch('magnum.cern.oidc.json.loads', side_effect=TestOIDC.mock_call("valid_id")):

                response = oidc_application._OidcApplication__getUserId(username="username")
                self.assertEquals(response, "TEST12345678901234567890123456789010")

                response = oidc_application._OidcApplication__getApplicationId(applicationIdentifier="application")
                self.assertEquals(response, "TEST12345678901234567890123456789010")

            # test invalid username/application id
            with mock.patch('magnum.cern.oidc.json.loads', side_effect=TestOIDC.mock_call("invalid_id")):
                self.assertRaises(exception.OIDCfailed, lambda: oidc_application._OidcApplication__getUserId(username="invalid"))
                self.assertRaises(exception.OIDCfailed, lambda: oidc_application._OidcApplication__getApplicationId(applicationIdentifier="invalid"))

            # test empty API data
            with mock.patch('magnum.cern.oidc.json.loads', side_effect=TestOIDC.mock_call("empty_data")):
                self.assertRaises(exception.OIDCfailed, lambda: oidc_application._OidcApplication__getUserId(username="username"))
                self.assertRaises(exception.OIDCfailed, lambda: oidc_application._OidcApplication__getApplicationId(applicationIdentifier="application"))
