#!/bin/sh

. /etc/sysconfig/heat-params

if [ "$CVMFS_ENABLED" = "False" ]; then
    exit 0
fi

chattr -i /
mkdir /cvmfs
chattr +i /

# allow containers to access fuse mounts
setsebool virt_sandbox_use_fusefs true

# make sure the cvmfs cache directory is created on the host
mkdir -p /var/cache/cvmfs

atomic install --name docker-volume-cvmfs --system gitlab-registry.cern.ch/cloud-infrastructure/docker-volume-cvmfs:${CVMFS_TAG:-latest}
systemctl start docker-volume-cvmfs
