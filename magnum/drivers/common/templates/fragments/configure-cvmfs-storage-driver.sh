#!/bin/sh

. /etc/sysconfig/heat-params

if [ "$CVMFS_STORAGE_DRIVER" = "False" ]; then
    exit 0
fi

docker plugin install --grant-all-permissions cvmfs/overlay2-graphdriver
sed -i "s#DOCKER_STORAGE_OPTIONS=.*#DOCKER_STORAGE_OPTIONS='--experimental -s cvmfs/overlay2-graphdriver'#" /etc/sysconfig/docker-storage
systemctl restart docker
