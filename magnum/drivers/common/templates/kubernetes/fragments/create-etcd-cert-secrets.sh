step="create-etcd-cert-secrets.sh"
printf "Starting to run ${step}\n"

. /etc/sysconfig/heat-params

echo "Waiting for Kubernetes API..."
until  [ "ok" = "$(kubectl get --raw='/healthz')" ]; do
    sleep 5
done

if [ -f /etc/etcd/certs/ca.crt ] && [ -f /etc/etcd/certs/server.crt ] && [ -f /etc/etcd/certs/server.key ] ; then
  kubectl -n kube-system create secret generic etcd-certificates --from-file=/etc/etcd/certs/ca.crt  --from-file=/etc/etcd/certs/server.crt --from-file=/etc/etcd/certs/server.key
fi
