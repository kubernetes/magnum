set +x
. /etc/sysconfig/heat-params
set -x
set -e

step="admin role binding creation for user"
echo "Starting to run ${step}"

if [ "$(echo "$OIDC_ENABLED" | tr '[:upper:]' '[:lower:]')" == "true" ]; then
    echo "Waiting for Kubernetes API..."
    until  [ "ok" = "$(kubectl get --raw='/healthz')" ]
    do
        sleep 5
    done
    kubectl create clusterrolebinding oidc-cluster-admin --clusterrole=cluster-admin --user="$OIDC_USERNAME"
fi
echo "Finished running ${step}"
