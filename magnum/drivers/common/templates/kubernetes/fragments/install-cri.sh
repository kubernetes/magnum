set +x

echo "START: install cri"

. /etc/sysconfig/heat-params
set -x

ssh_cmd="ssh -F /srv/magnum/.ssh/config root@localhost"

if [ "${CONTAINER_RUNTIME}" = "containerd"  ] ; then
    $ssh_cmd systemctl disable docker
    if [ -z "${CONTAINERD_TARBALL_URL}"  ] ; then
        CONTAINERD_TARBALL_URL="https://storage.googleapis.com/cri-containerd-release/cri-containerd-${CONTAINERD_VERSION}.linux-amd64.tar.gz"
    fi
    i=0
    until curl -L -o /srv/magnum/cri-containerd.tar.gz "${CONTAINERD_TARBALL_URL}"
    do
        i=$((i + 1))
        [ $i -lt 5 ] || break;
        sleep 5
    done

    $ssh_cmd tar xzvf /srv/magnum/cri-containerd.tar.gz -C / --no-same-owner --touch --no-same-permissions

    kube_min_version=$(echo "${KUBE_TAG}" | cut -d'.' -f2)
    if [ "$kube_min_version" -ge "24" ]; then
        cat > /etc/containerd/config.toml <<EOF
version = 2
root = "/var/lib/containerd"

[plugins]
  [plugins."io.containerd.grpc.v1.cri"]
    sandbox_image = "${CONTAINER_INFRA_PREFIX:-k8s.gcr.io/}pause:3.10"
EOF
    fi

    $ssh_cmd systemctl daemon-reload
    $ssh_cmd systemctl enable containerd
    $ssh_cmd systemctl start containerd
else
    # CONTAINER_RUNTIME=host-docker
    $ssh_cmd systemctl disable docker
    if $ssh_cmd cat /usr/lib/systemd/system/docker.service | grep 'native.cgroupdriver'; then
            $ssh_cmd cp /usr/lib/systemd/system/docker.service /etc/systemd/system/
            sed -i "s/\(native.cgroupdriver=\)\w\+/\1$CGROUP_DRIVER/" \
                    /etc/systemd/system/docker.service
    else
            cat > /etc/systemd/system/docker.service.d/cgroupdriver.conf << EOF
    ExecStart=---exec-opt native.cgroupdriver=$CGROUP_DRIVER
EOF
    fi
    sed -i -E 's/--default-ulimit nofile=1024:1024/--default-ulimit nofile=1048576:1048576/' /etc/sysconfig/docker

    $ssh_cmd systemctl daemon-reload
    $ssh_cmd systemctl enable docker
fi

echo "END: install cri"
