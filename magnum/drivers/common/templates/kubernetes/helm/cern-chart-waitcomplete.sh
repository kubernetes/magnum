#!/bin/bash

. /etc/sysconfig/heat-params

set -ex

step="install-helm-modules"
printf "Starting to run ${step}\n"

if [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 14 ) ]]; then

    echo "Waiting for Kubernetes API..."
    until  [ "ok" = "$(kubectl get --raw='/healthz')" ]
    do
        sleep 5
    done

    # wait for the install job to finish
    kubectl -n magnum-tiller wait job/install-cern-magnum-job --for=condition=complete --timeout=${HELM_INSTALL_TIMEOUT}

    # check all resources are ready
    for r in $(kubectl -n kube-system get deploy -o=jsonpath='{.items[*].metadata.name}'); do
      kubectl -n kube-system wait deploy/${r} --for=condition=Available=True --timeout=600s
    done
    for r in $(kubectl -n kube-system get daemonset -o=jsonpath='{.items[*].metadata.name}'); do
      desired=$(kubectl -n kube-system get daemonset $r -o=jsonpath='{.status.desiredNumberScheduled}')
      ready=$(kubectl -n kube-system get daemonset $r -o=jsonpath='{.status.numberReady}')
      while [ "${desired}" != "${ready}" ]; do
        desired=$(kubectl -n kube-system get daemonset $r -o=jsonpath='{.status.desiredNumberScheduled}')
        ready=$(kubectl -n kube-system get daemonset $r -o=jsonpath='{.status.numberReady}')
        sleep 5
      done
    done
fi

printf "Finished running ${step}\n"
