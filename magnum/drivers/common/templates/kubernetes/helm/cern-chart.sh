#!/bin/bash

. /etc/sysconfig/heat-params

set -ex

step="cern-chart"
printf "Starting to run ${step}\n"

### Configure installation dependencies
###############################################################################
KUBE_PROMETHEUS_SERVER_CPU="$(expr 128 + 7 \* ${MAX_NODE_COUNT} )m"
KUBE_PROMETHEUS_SERVER_MEMORY="$(expr 256 + 40 \* ${MAX_NODE_COUNT})M"
# KUBE_PROMETHEUS_SERVER_CPU and KUBE_PROMETHEUS_SERVER_MEMORY are not a magnum value, need to write it in heat-params
echo "KUBE_PROMETHEUS_SERVER_CPU=\"${KUBE_PROMETHEUS_SERVER_CPU}\"" >> /etc/sysconfig/heat-params
echo "KUBE_PROMETHEUS_SERVER_MEMORY=\"${KUBE_PROMETHEUS_SERVER_MEMORY}\"" >> /etc/sysconfig/heat-params

if [ "$(echo ${MONITORING_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ] && \
   [ "$(echo ${METRICS_PRODUCER})" != "" ]; then
    CERN_CENTRAL_MONITORING="true"
else
    CERN_CENTRAL_MONITORING="false"
fi
# CERN_CENTRAL_MONITORING is not a magnum value, need to write it in heat-params
echo "CERN_CENTRAL_MONITORING=\"${CERN_CENTRAL_MONITORING}\"" >> /etc/sysconfig/heat-params

if [ "$(echo ${MONITORING_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ] && \
   [ "$(echo ${PROMETHEUS_ADAPTER_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ]; then
    PROMETHEUS_ADAPTER_HELM_ENABLED="true"
else
    PROMETHEUS_ADAPTER_HELM_ENABLED="false"
fi
# PROMETHEUS_ADAPTER_HELM_ENABLED is not a magnum value, need to write it in heat-params
echo "PROMETHEUS_ADAPTER_HELM_ENABLED=\"${PROMETHEUS_ADAPTER_HELM_ENABLED}\"" >> /etc/sysconfig/heat-params

if [ "$(echo ${LOGGING_INSTALLER} | tr '[:upper:]' '[:lower:]')" = "helm" ] && \
   [ ! -z "${LOGGING_PRODUCER}" ]; then
    CERN_CENTRAL_LOGGING="true"
else
    CERN_CENTRAL_LOGGING="false"
fi
# CERN_CENTRAL_LOGGING is not a magnum value, need to write it in heat-params
echo "CERN_CENTRAL_LOGGING=\"${CERN_CENTRAL_LOGGING}\"" >> /etc/sysconfig/heat-params

NVIDIA_GPU_VALUES=""
if [ "$(echo ${NVIDIA_GPU_TAG})" != "" ]; then
    NVIDIA_GPU_VALUES="
      plugin:
        image:
          tag: ${NVIDIA_GPU_TAG}
      installer:
        image:
          tag: ${NVIDIA_GPU_TAG}
"
fi
# NVIDIA_GPU_VALUES is not a magnum value, need to write it in heat-params
echo "NVIDIA_GPU_VALUES=\"${NVIDIA_GPU_VALUES}\"" >> /etc/sysconfig/heat-params

if [ "$(echo ${INGRESS_CONTROLLER} | tr '[:upper:]' '[:lower:]')" == "nginx" ] && \
   [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 9 ) ]]; then
    CERN_INGRESS_NGINX_ENABLED="true"
else
    CERN_INGRESS_NGINX_ENABLED="false"
fi
# CERN_INGRESS_NGINX_ENABLED is not a magnum value, need to write it in heat-params
echo "CERN_INGRESS_NGINX_ENABLED=\"${CERN_INGRESS_NGINX_ENABLED}\"" >> /etc/sysconfig/heat-params

AUTO_SCALING_ENABLED=$(echo $AUTO_SCALING_ENABLED | tr '[:upper:]' '[:lower:]')
AUTO_HEALING_ENABLED=$(echo $AUTO_HEALING_ENABLED | tr '[:upper:]' '[:lower:]')
AUTO_HEALING_CONTROLLER=$(echo ${AUTO_HEALING_CONTROLLER} | tr '[:upper:]' '[:lower:]')
if [[ "${AUTO_SCALING_ENABLED}" = "true" || ("${AUTO_HEALING_ENABLED}" = "true" && "${AUTO_HEALING_CONTROLLER}" = "draino") ]]; then
  CLUSTER_AUTOSCALER_ENABLED="true"
else
  CLUSTER_AUTOSCALER_ENABLED="false"
fi
# CLUSTER_AUTOSCALER_ENABLED is not a magnum value, need to write it in heat-params
echo "CLUSTER_AUTOSCALER_ENABLED=\"${CLUSTER_AUTOSCALER_ENABLED}\"" >> /etc/sysconfig/heat-params

if [ "$(echo ${INGRESS_CONTROLLER} | tr '[:upper:]' '[:lower:]')" == "traefik" ] && \
   [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 9 ) ]]; then
    TRAEFIK_HELM_ENABLED="true"
else
    TRAEFIK_HELM_ENABLED="false"
fi
# TRAEFIK_HELM_ENABLED is not a magnum value, need to write it in heat-params
echo "TRAEFIK_HELM_ENABLED=\"${TRAEFIK_HELM_ENABLED}\"" >> /etc/sysconfig/heat-params

if [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 12 ) ]]; then
    COREDNS_HELM_ENABLED="true"
else
    COREDNS_HELM_ENABLED="false"
fi
# COREDNS_HELM_ENABLED is not a magnum value, need to write it in heat-params
echo "COREDNS_HELM_ENABLED=\"${COREDNS_HELM_ENABLED}\"" >> /etc/sysconfig/heat-params

if [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 12 ) ]]; then
    OCCM_HELM_ENABLED="${CLOUD_PROVIDER_ENABLED}"
else
    OCCM_HELM_ENABLED="false"
fi
# OCCM_HELM_ENABLED is not a magnum value, need to write it in heat-params
echo "OCCM_HELM_ENABLED=\"${OCCM_HELM_ENABLED}\"" >> /etc/sysconfig/heat-params

if [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 12 ) && ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -lt 18 )]]; then
    CALICO_HELM_ENABLED="true"
else
    CALICO_HELM_ENABLED="false"
fi
# CALICO_HELM_ENABLED is not a magnum value, need to write it in heat-params
echo "CALICO_HELM_ENABLED=\"${CALICO_HELM_ENABLED}\"" >> /etc/sysconfig/heat-params

if [ "$IP_FAMILY_POLICY" = "dual_stack"  ]; then
    IPV6_ENABLED="true"
else
    IPV6_ENABLED="false"
fi
# IPV6_ENABLED is not a magnum value, need to write it in heat-params
echo "IPV6_ENABLED=\"${IPV6_ENABLED}\"" >> /etc/sysconfig/heat-params

LANDB_SET_ENABLED=$(if [ "$LANDB_SYNC_SET" != "" ]; then echo "true"; else echo "false"; fi)
# LANDB_SET_ENABLED is not a magnum value, need to write it in heat-params
echo "LANDB_SET_ENABLED=\"${LANDB_SET_ENABLED}\"" >> /etc/sysconfig/heat-params

OPENSTACK_KEYSTONE_AUTH_URL=$(echo ${AUTH_URL} | cut -d'/' -f3)
OCCM_INIT_CONTAINER_IMAGE=${CONTAINER_INFRA_PREFIX:-docker.io/library/}busybox
# OPENSTACK_KEYSTONE_AUTH_URL is not a magnum value, need to write it in heat-params
echo "OPENSTACK_KEYSTONE_AUTH_URL=\"${OPENSTACK_KEYSTONE_AUTH_URL}\"" >> /etc/sysconfig/heat-params
echo "OCCM_INIT_CONTAINER_IMAGE=\"${OCCM_INIT_CONTAINER_IMAGE}\"" >> /etc/sysconfig/heat-params

HELM_CLIENT="helm-client"
# (rocha) we do this as we had v3.2.0 hard-coded and old templates had helm tag badly set to 2.6
HELM_TAG="v3.2.0"
if [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 13 ) ]]; then
	HELM_CLIENT="helm"
	HELM_TAG="$HELM_CLIENT_TAG"
fi

### Configuration
###############################################################################
CHART_NAME="cern-magnum"
CRD_CHART_NAME=${CHART_NAME}-crds
_cern_chart_values_file="/srv/magnum/kubernetes/cern_chart_values.yaml"
_cern_chart_user_values_file="/srv/magnum/kubernetes/cern_chart_user_values.yaml"

if [ "$(echo ${CERN_CHART_VALUES})" != "" ]; then
    # decode base64 and indent by 4 spaces to write the ConfigMap below
    echo ${CERN_CHART_VALUES} | base64 -d | sed 's/^/    /g'> ${_cern_chart_values_file}
    # backwards compatible with labels happens later with envsubst
    # envsubst does not work with shell variables, it works only with environment variables.
    while read line
    do
        heat_param_name=$(echo $line | cut -d"=" -f1)
        heat_param_value=$(echo $line | cut -d"=" -f2 | sed 's/"//g')
        sed -i "s#\${${heat_param_name}}#${heat_param_value}#g" ${_cern_chart_values_file}
    done < /etc/sysconfig/heat-params
else
    cat <<EOF > ${_cern_chart_values_file}
    eosxd:
      enabled: ${EOS_ENABLED}
    # nvidia-gpu is no longer required with >=1.24 clusters, we use gpu-operator instead
    nvidia-gpu:
      enabled: ${NVIDIA_GPU_ENABLED}
${NVIDIA_GPU_VALUES}
    gpu-operator:
      enabled: ${NVIDIA_GPU_ENABLED}
    fluentd:
      enabled: ${CERN_CENTRAL_LOGGING}
      output:
        producer: ${LOGGING_PRODUCER}
        endpoint: ${LOGGING_HTTP_DESTINATION}
        includeInternal: ${LOGGING_INCLUDE_INTERNAL}
      containerRuntime: ${CONTAINER_RUNTIME}
    landb-sync:
      # We need this to have backwards compatibility
      enabled: ${LANDB_SYNC_ENABLED}
      landbAlias:
        enabled: ${LANDB_SYNC_ENABLED}
      landbSet:
        enabled: ${LANDB_SET_ENABLED}
        name: ${LANDB_SYNC_SET}
    prometheus-cern:
      enabled: ${CERN_CENTRAL_MONITORING}
    ceph-csi-cephfs:
      enabled: ${CEPHFS_CSI_ENABLED}
    openstack-cinder-csi:
      enabled: ${CINDER_CSI_ENABLED}
      clusterID: ${CLUSTER_UUID}
    openstack-manila-csi:
      enabled: ${MANILA_CSI_ENABLED}
      csimanila:
        clusterID: ${CLUSTER_UUID}
    cert-manager:
      enabled: ${CERT_MANAGER_IO_ENABLED}
    snapshot-controller:
      enabled: ${SNAPSHOT_CONTROLLER_ENABLED}
    snapshot-validation-webhook:
      enabled: ${SNAPSHOT_VALIDATION_WEBHOOK_ENABLED}
    node-problem-detector:
      enabled: ${NPD_ENABLED}
    coredns:
      enabled: ${COREDNS_HELM_ENABLED}
      service:
        clusterIP: ${DNS_SERVICE_IP}
      # passing all plugins below as this is a list and it replaces the default cern-magnum values
      servers:
      - zones:
        port: 53
        plugins:
        - name: cache
          parameters: 900
        - name: errors
        - name: forward
          parameters: . /etc/resolv.conf
        - name: health
        - name: kubernetes
          parameters: ${DNS_CLUSTER_DOMAIN} ${PORTAL_NETWORK_CIDR} ${PODS_NETWORK_CIDR}
          configBlock: |-
            pods verified
            fallthrough in-addr.arpa ip6.arpa
        - name: loadbalance
        - name: log
          parameters: stdout
        - name: prometheus
          parameters: :9153
        - name: ready
        - name: reload
    openstack-cloud-controller-manager:
      enabled: ${OCCM_HELM_ENABLED}
      image:
        tag: ${CLOUD_PROVIDER_TAG}
      extraInitContainers:
      - name: init-waitkeystone
        image: ${OCCM_INIT_CONTAINER_IMAGE}
        command: ["sh", "-c", "until nslookup ${OPENSTACK_KEYSTONE_AUTH_URL}; do echo waiting for keystone; sleep 2; done;"]
      controllerExtraArgs: |-
        - --v=2
        - --cloud-config=/etc/kubernetes/cloud-config-occm
        - --use-service-account-credentials=true
        - --bind-address=127.0.0.1
        - --cluster-name=${CLUSTER_UUID}
    base:
      enabled: ${CERN_ENABLED}
    cern-authz:
      enabled: ${CERN_ENABLED}
    ingress-nginx:
      enabled: ${CERN_INGRESS_NGINX_ENABLED}
      controller:
        image:
          tag: ${NGINX_INGRESS_CONTROLLER_TAG}
        nodeSelector:
          role: ${INGRESS_CONTROLLER_ROLE}
        metrics:
          enabled: ${MONITORING_ENABLED}
          serviceMonitor:
            enabled: ${MONITORING_ENABLED}
    traefik:
      enabled: ${TRAEFIK_HELM_ENABLED}
      image:
        tag: ${TRAEFIK_INGRESS_CONTROLLER_TAG}
      nodeSelector:
        role: ${INGRESS_CONTROLLER_ROLE}
    metrics-server:
      enabled: ${METRICS_SERVER_ENABLED}
    kube-prometheus-stack:
      enabled: ${MONITORING_ENABLED}
      grafana:
        adminPassword: ${GRAFANA_ADMIN_PASSWD}
      prometheus:
        prometheusSpec:
          externalLabels:
            cluster_uuid: ${CLUSTER_UUID}
          secrets:
          - etcd-certificates
          resources:
            requests:
              cpu: ${KUBE_PROMETHEUS_SERVER_CPU}
              memory: ${KUBE_PROMETHEUS_SERVER_MEMORY}
    prometheus-adapter:
      enabled: ${PROMETHEUS_ADAPTER_HELM_ENABLED}
      rules:
        existing: ${PROMETHEUS_ADAPTER_CONFIGMAP}
    cvmfs-csi:
      enabled: ${CVMFS_CSI_ENABLED}
    kubernetes-dashboard:
      enabled: ${KUBE_DASHBOARD_ENABLED}
      serviceMonitor:
        enabled: ${MONITORING_ENABLED}
    calico:
      enabled: ${CALICO_HELM_ENABLED}
      image:
        tag: ${CALICO_TAG}
      ipip: ${CALICO_IPV4POOL_IPIP}
      ipv4:
        enabled: true
        cidr: ${CALICO_IPV4POOL}
      ipv6:
        enabled: ${IPV6_ENABLED}
        cidr: ${PODS_NETWORK6_CIDR}
    cluster-autoscaler:
      enabled: ${CLUSTER_AUTOSCALER_ENABLED}
      magnumClusterName: ${CLUSTER_UUID}
      autoscalingGroups:
      - name: default-worker
        maxSize: ${MAX_NODE_COUNT}
        minSize: ${MIN_NODE_COUNT}
      serviceMonitor:
        enabled: ${MONITORING_ENABLED}
EOF
fi

if [ "$(echo ${CERN_CHART_USER_VALUES})" != "" ]; then
    # decode base64 and indent by 4 spaces to write the ConfigMap below
    echo ${CERN_CHART_USER_VALUES} | base64 -d | sed 's/^/    /g'> ${_cern_chart_user_values_file}
else
  echo "" > ${_cern_chart_user_values_file}
fi

HELM_INSTALL_TIMEOUT=$(grep "helm-install-timeout" ${_cern_chart_user_values_file} | tail -n1 | awk '{ print $2}')
if [ -z "${HELM_INSTALL_TIMEOUT}" ]; then
  HELM_INSTALL_TIMEOUT=$(grep "helm-install-timeout" ${_cern_chart_values_file} | tail -n1 | awk '{ print $2}')
fi
HELM_INSTALL_TIMEOUT="${HELM_INSTALL_TIMEOUT:="45m0s"}"
# HELM_INSTALL_TIMEOUT is not a magnum value, need to write it in heat-params
echo "HELM_INSTALL_TIMEOUT=\"${HELM_INSTALL_TIMEOUT}\"" >> /etc/sysconfig/heat-params

if [ "$(echo ${CERN_CHART_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ]; then

    # if tiller_enabled is not true catch error
    if [ "$(echo ${TILLER_ENABLED} | tr '[:upper:]' '[:lower:]')" != "true" ]; then
        echo "ERROR: 'TILLER_ENABLED' must be true when 'cern_chart' is enabled, current value: " + ${TILLER_ENABLED}
        exit 1
    fi

    HELM_MODULE_CONFIG_FILE="/srv/magnum/kubernetes/helm/${CHART_NAME}.yaml"
    [ -f ${HELM_MODULE_CONFIG_FILE} ] || {
        echo "Writing File: ${HELM_MODULE_CONFIG_FILE}"
        mkdir -p $(dirname ${HELM_MODULE_CONFIG_FILE})
        cat << EOF > ${HELM_MODULE_CONFIG_FILE}
---
apiVersion: v1
kind: Secret
metadata:
  name: ${CHART_NAME}-config
  namespace: magnum-tiller
  labels:
    app: helm
stringData:
  install-${CHART_NAME}.sh: |
    #!/bin/bash
    set -ex
    mkdir -p \${HELM_HOME}
    cp /etc/helm/* \${HELM_HOME}

    CHART_REPO="oci://$(echo ${CONTAINER_INFRA_PREFIX} | sed -n 's#[http(s)://]*\(.*cern.ch\).*#\1#p')/kubernetes/charts"
    if [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -le 13 ) ]]; then
        CHART_REPO="releases"
        helm repo add releases https://$(echo ${CONTAINER_INFRA_PREFIX} | sed -n 's#[http(s)://]*\(.*cern.ch\).*#\1#p')/chartrepo/releases
        helm repo update
    fi

    if [[ \$(helm history ${CHART_NAME} | grep ${CHART_NAME}) ]]; then
        echo "${CHART_NAME} already installed on server. Continue..."
        exit 0
    else
        repo=\$(echo ${CERN_CHART_VERSION} | sed -rn "s/.*###(.*)###(.*)/\1/p")
        branch=\$(echo ${CERN_CHART_VERSION} | sed -rn "s/.*###(.*)###(.*)/\2/p")

        if [ "\${branch}" != "" ]; then
            rm -rf cern-magnum-\${branch}*
            curl -O \${repo}/-/archive/\${branch}/cern-magnum-\${branch}.tar.gz
            tar zxvf cern-magnum-\${branch}.tar.gz
            curl -L "https://github.com/mikefarah/yq/releases/download/v4.31.2/yq_linux_amd64" -o /usr/bin/yq && chmod +x /usr/bin/yq

            if [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 17 ) ]]; then
              cd cern-magnum-\${branch}
              CERN_MAGNUM_CHART_NAME=${CHART_NAME} CERN_MAGNUM_CRD_CHART_NAME=${CRD_CHART_NAME} ./package.sh
              helm -n kube-system install ${CRD_CHART_NAME} ./build/${CRD_CHART_NAME} --wait --timeout ${HELM_INSTALL_TIMEOUT}
              helm -n kube-system install ${CHART_NAME} ./build/${CHART_NAME} --wait --timeout ${HELM_INSTALL_TIMEOUT} --values /opt/magnum/install-${CHART_NAME}-values.yaml --values /opt/magnum/install-${CHART_NAME}-user-values.yaml
            else # Backwards compatibility support for cern-magnum as a single chart.
              mkdir -p cern-magnum-\${branch}/crds
              helm dep update cern-magnum-\${branch}
              helm -n kube-system template -f cern-magnum-\${branch}/values-crds.yaml cern-magnum-\${branch} | 
                yq e 'select(.kind == "CustomResourceDefinition")' - > cern-magnum-\${branch}/crds/generated.yaml

              for d in \$(cat cern-magnum-\${branch}/Chart.yaml | yq '.dependencies[].name'); do 
                if tar zxf cern-magnum-\${branch}/charts/\${d}*tgz; then 
                  cat \${d}/crds/*yaml >> cern-magnum-\${branch}/crds/generated.yaml 2>/dev/null || true; 
                fi 
                rm -rf \${d}
              done

              helm -n kube-system install ${CHART_NAME} ./cern-magnum-\${branch} --wait --timeout ${HELM_INSTALL_TIMEOUT} --values /opt/magnum/install-${CHART_NAME}-values.yaml --values /opt/magnum/install-${CHART_NAME}-user-values.yaml
            fi
        else
            if [[ ( $(echo ${CERN_CHART_VERSION} | cut -d. -f2) -ge 17 ) ]]; then
              helm -n kube-system install ${CRD_CHART_NAME} \${CHART_REPO}/${CRD_CHART_NAME} --wait --timeout ${HELM_INSTALL_TIMEOUT} --version ${CERN_CHART_VERSION}
              helm -n kube-system install ${CHART_NAME} \${CHART_REPO}/${CHART_NAME} --wait --timeout ${HELM_INSTALL_TIMEOUT} --version ${CERN_CHART_VERSION} --values /opt/magnum/install-${CHART_NAME}-values.yaml --values /opt/magnum/install-${CHART_NAME}-user-values.yaml
            else
              helm -n kube-system install ${CHART_NAME} \${CHART_REPO}/${CHART_NAME} --wait --timeout ${HELM_INSTALL_TIMEOUT} --version ${CERN_CHART_VERSION} --values /opt/magnum/install-${CHART_NAME}-values.yaml --values /opt/magnum/install-${CHART_NAME}-user-values.yaml
            fi
        fi
    fi

  install-${CHART_NAME}-values.yaml: |
$(cat ${_cern_chart_values_file})

  install-${CHART_NAME}-user-values.yaml: |
$(cat ${_cern_chart_user_values_file})
---
apiVersion: batch/v1
kind: Job
metadata:
  name: install-${CHART_NAME}-job
  namespace: magnum-tiller
spec:
  backoffLimit: 0
  template:
    spec:
      serviceAccountName: tiller
      containers:
      - name: config-helm
        image: ${CONTAINER_INFRA_PREFIX:-docker.io/openstackmagnum/}${HELM_CLIENT}:${HELM_TAG}
        command:
        - bash
        args:
        - /opt/magnum/install-${CHART_NAME}.sh
        env:
        - name: HELM_HOME
          value: /helm_home
        - name: TILLER_NAMESPACE
          value: magnum-tiller
        - name: HELM_TLS_ENABLE
          value: "true"
        volumeMounts:
        - name: install-${CHART_NAME}-config
          mountPath: /opt/magnum/
        - mountPath: /etc/helm
          name: helm-client-certs
      hostNetwork: true
      restartPolicy: Never
      tolerations:
      - effect: NoSchedule
        key: node.cloudprovider.kubernetes.io/uninitialized
        value: "true"
      - effect: NoSchedule
        operator: Exists
      - key: CriticalAddonsOnly
        operator: Exists
      - effect: NoExecute
        operator: Exists
      nodeSelector:
        node-role.kubernetes.io/master: ""
      volumes:
      - name: install-${CHART_NAME}-config
        secret:
          secretName: ${CHART_NAME}-config
      - name: helm-client-certs
        secret:
          secretName: helm-client-secret
EOF
    }

fi

printf "Finished running ${step}\n"
